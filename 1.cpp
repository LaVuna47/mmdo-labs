/** gnu specific **/
//#include <ext/pb_ds/assoc_container.hpp>
//#include <ext/pb_ds/tree_policy.hpp>
//using namespace __gnu_pbds;
/** c++ standart **/
#include <vector>
//#include <queue>
//#include <unordered_map>
//#include <sstream>
//#include <list>
#include <iostream>
//#include <iomanip>
//#include <algorithm>
//#include <stack>
#include <unordered_set>
//#include <set>
//#include <map>
//#include <functional>
//#include <list>
//#include <iomanip>
//#include <complex>
//#include <random>
//#include <fstream>
//#include <functional>
/** c standart **/
//#include <bits/stdc++.h>
//#include <cstdio>
//#include <cmath>
//#include <cstring>
//#include <algorithm>
#include <string>
#define  PI (3.14159265358979323846264338327950288419716939937510)
#define  fo(i, n) for(ll i = 0; i < n; ++i)
#define  ro(i, n) for(ll i = n - 1; i >= 0; --i)
#define  all(x) (x).begin(), (x).end()
#define  MAX_FLOAT 2000000000
using namespace std;
typedef long long ll;
typedef vector<ll> vll;
typedef unsigned long long ull;
typedef pair<ll, ll> pll;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<bool> vb;
typedef short si;
typedef unsigned int uint;
typedef long double LD;
typedef pair<ull, ull> pull;
typedef pair<si, si> psii;

#define MAX_N 100
#define MAX_M 100
#define MAX_MONEY 100
#define INFINITY 1000000000

int cost[MAX_N][MAX_M];
double profit[MAX_N][MAX_M];

struct triple{
    int x,y,z;
    triple()=default;
    triple(int x, int y, int z):
        x(x), y(y), z(z) { }
    bool operator!=(const triple& tr) const
    {
        return x != tr.x || y != tr.y || z != tr.z;
    }
};
double dp[MAX_N][MAX_M][MAX_MONEY];
triple pr[MAX_N][MAX_M][MAX_MONEY];

double f(int i, int j, int money){
    if(money < 0.)
        return -INFINITY;
    if(i == -1 || j == -1)
        return 0;
    if(dp[i][j][money] != -1)
        return dp[i][j][money];
    double cur_res_1 = f(i - 1, j - 1,money - cost[i][j]) + profit[i][j];
    double cur_res_2 = f(i - 1, j, money);
    double cur_res_3 = f(i,j - 1, money);
    if(cur_res_1 > cur_res_2 && cur_res_1 > cur_res_3)
    {
        pr[i][j][money] = {i - 1, j - 1, money - cost[i][j]};
        dp[i][j][money] = cur_res_1;
    }
    else if(cur_res_2 > cur_res_3)
    {
        pr[i][j][money] = {i - 1, j, money};
        dp[i][j][money] = cur_res_2;
    }
    else
    {
        pr[i][j][money] = {i, j - 1, money};
        dp[i][j][money] = cur_res_3;
    }
    return dp[i][j][money];
}

void prepare()
{
    for(auto& item: dp)
    {
        for(auto& j: item)
        {
            for(auto& i: j)
                i = -1;
        }
    }
    for(auto& item: pr)
    {
        for(auto& j: item)
        {
            for(auto& i: j)
                i = {-1,-1,-1};
        }
    }
}

void printOptimalProjectsAndCompanies(int i, int j, int m)
{
    triple st = {i, j, m};
    int I = 0;
    while(st.x != -1 && st.y != -1 && st.z != -1)
    {
        if(st.z != pr[st.x][st.y][st.z].z) {
            cout << "proj: " << st.x + 1 << '\n';
            cout << "company: " << st.y + 1 << '\n';
            cout << "spend money: " << st.z - pr[st.x][st.y][st.z].z << '\n';
        }
        st = pr[st.x][st.y][st.z];
        cout << '\n';
    }
}

/*
 ! One company can have maximum one project !
 */
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    prepare();

    int n, m, money;
    cin >> n >> m >> money;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> cost[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> profit[i][j];
        }
    }

    cout << "Max Profit: " << f(n - 1,m - 1,money) << '\n';
    cout << "Optimal solution:\n";
    printOptimalProjectsAndCompanies(n - 1, m - 1, money);
}
/*
Tests:
 5 3 8

 3 1 3
 2 4 3
 1 1 1
 5 5 4
 5 3 5

 0.8 0.3 0.8
 0.4 0.8 0.6
 0.4 0.4 0.4
 1.5 1.5 1.2
 1.3 0.8 1.3
 */


