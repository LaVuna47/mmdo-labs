/** gnu specific **/
//#include <ext/pb_ds/assoc_container.hpp>
//#include <ext/pb_ds/tree_policy.hpp>
//using namespace __gnu_pbds;
/** c++ standart **/
#include <vector>
//#include <queue>
//#include <unordered_map>
//#include <sstream>
//#include <list>
#include <iostream>
//#include <iomanip>
//#include <algorithm>
//#include <stack>
#include <unordered_set>
//#include <set>
//#include <map>
//#include <functional>
//#include <list>
//#include <iomanip>
//#include <complex>
//#include <random>
//#include <fstream>
//#include <functional>
/** c standart **/
//#include <bits/stdc++.h>
//#include <cstdio>
//#include <cmath>
//#include <cstring>
//#include <algorithm>
#include <string>
#define  PI (3.14159265358979323846264338327950288419716939937510)
#define  fo(i, n) for(ll i = 0; i < n; ++i)
#define  ro(i, n) for(ll i = n - 1; i >= 0; --i)
#define  all(x) (x).begin(), (x).end()
#define  MAX_FLOAT 2000000000
using namespace std;
typedef long long ll;
typedef vector<ll> vll;
typedef unsigned long long ull;
typedef pair<ll, ll> pll;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<bool> vb;
typedef short si;
typedef unsigned int uint;
typedef long double LD;
typedef pair<ull, ull> pull;
typedef pair<si, si> psii;

#define INFINITY 1000000000

vector<int> cost;
vector<double> profit;
vector<vector<double>> dp;
vector<vector<pii>> pr;
double f(int n, int money)
{
    if(money < 0) return -INFINITY;
    if(n == -1) return 0;
    if(dp[n][money] != -1)
        return dp[n][money];
    double res1 = f(n - 1, money);
    double res2 = f(n - 1,money - cost[n]) + profit[n];
    if(res1 > res2)
    {
        pr[n][money] = {n - 1, money};
        return dp[n][money] = res1;
    }
    else
    {
        pr[n][money] = {n - 1, money - cost[n]};
        return dp[n][money] = res2;
    }
}

void printOptimalProjectsAndCompanies(int n, int money, int I, int J)
{
    pii st = {n, money};
    while(st.first != -1)
    {
        if(pr[st.first][st.second].second < st.second)
        {
            cout << "proj: " << (st.first / J) + 1 << '\n';
            cout << "company: " << (st.first % J) + 1 << '\n';
            cout << "spend money: " << st.second - pr[st.first][st.second].second<< '\n';
            cout << '\n';
        }
        st = pr[st.first][st.second];
    }

}

/*
 ! One company may handle all projects !
 */
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int n, m, money;
    cin >> n >> m >> money;

    // reserving space
    cost = vector<int>(n * m + 1,0);
    profit = vector<double>(n * m + 1,0);
    dp = vector<vector<double>>(n*m + 1,vector<double>(money + 1,-1.));
    pr = vector<vector<pii>>(n * m + 1,vector<pii>(money + 1,{-1,-1}));
    // reading another data
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> cost[i * m + j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> profit[i * m + j];
        }
    }
    cout << "Max profit: " << f(n * m - 1,money) << '\n';
    cout << "Optimal solution:\n";
    printOptimalProjectsAndCompanies(n * m - 1, money, n, m);
}
/*
Tests:
 5 3 8

 3 1 3
 2 4 3
 1 1 1
 5 5 4
 5 3 5

 0.8 0.3 0.8
 0.4 0.8 0.6
 0.4 0.4 0.4
 1.5 1.5 1.2
 1.3 0.8 1.3
 */






